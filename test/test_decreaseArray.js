const { expect } = require('chai')
const { decreaseArray } = require('../src/index')

describe('Checking decreaseArray function', () => {
  it('should handle not array', () => {
    expect(decreaseArray('string')).to.be.equal(undefined)
  })
  it('should handle array with NaN', () => {
    expect(decreaseArray([1, Number.NaN, 4, 8])).to.be.equal(undefined)
  })
  it('should handle empty array', () => {
    expect(decreaseArray([]).length).to.be.equal(0)
  })
  it('should process the array without duplicate values', () => {
    const arr = [1, 2, 4, 8]
    expect(decreaseArray(arr).length).to.be.equal(arr.length)
  })
  it('should process the array without repeated values side by side', () => {
    const arr = [1, 2, 4, 8, 4, 2, 1]
    expect(decreaseArray(arr).length).to.be.equal(arr.length)
  })
  it('should decrease array length by summing repeated values', () => {
    const arr = [1, 4, 8, 8, 16, 2, 1]
    const res = [1, 4, 32, 2, 1]
    expect(decreaseArray(arr).length).to.be.equal(res.length)
  })
  it('should decrease array length by summing repeated values', () => {
    const arr = [1, 1, 2, 4, 8, 16, 32]
    const res = [64]
    expect(decreaseArray(arr).length).to.be.equal(res.length)
  })
  it('should decrease array length by summing repeated values', () => {
    const arr = [32, 16, 8, 4, 2, 1, 1]
    const res = [64]
    expect(decreaseArray(arr).length).to.be.equal(res.length)
  })
})
