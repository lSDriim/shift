const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin
})

rl.on('line', function (line) {
  /*
   * Если line будет пустая то функция сгенерирует массив [ 0 ],
   * про этот случай ничего в описании не было потому оставил так
   */
  let array = line.split(',').map(Number)

  array = decreaseArray(array)

  console.log(array)
})

function decreaseArray (array) {
  let flag = true

  /* проверяем что был передан массив и что он не содержит значений NaN */
  if (!Array.isArray(array) || array.findIndex(i => isNaN(i)) !== -1) {
    console.log('incorrect data')
    return undefined
  }

  while (flag) {
    let pass
    flag = false

    array = array.reduce((acc, value, index, arr) => {
      if (index + 1 < arr.length && value === arr[index + 1]) {
        flag = true /* если за проход не будет произведено ни одного сдвига то мы закончим обработку */
        pass = index + 1
        acc.push(value * 2)
      } else if (index !== pass) {
        acc.push(value)
      }

      return acc
    }, [])
  }

  return array
}

exports.decreaseArray = decreaseArray
